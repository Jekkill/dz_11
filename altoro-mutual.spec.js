import chai from 'chai';
import {goto, run, stop} from "./framework/lib/browser";

const {expect} = chai;

describe('Test suites for AltoroMutual site', () => {

    let page;
    before(async () => {
        await run();
        page = await goto('http://demo.testfire.net/index.jsp');
    });

    after(async () => {
        await stop();
    });

    it('Should successfully login', async () => {
        await page.click('#LoginLink');
        await page.click('#uid');
        await page.fill('#uid', 'admin');
        await page.click('#passw');
        await page.fill('#passw', 'admin');
        await page.click('input[name=\'btnSubmit\']');
        const profileName = ('.bb > .fl > h1');
        const profileNameText = await page.textContent(profileName);
        expect(profileNameText).to.have.string('Admin');
    });

    it('Should show the account history', async () => {
        const accountHistorySelector = ('#listAccounts > option:first-child');
        const accountHistoryText = await page.textContent(accountHistorySelector);
        await page.click('#btnGetAccount');
        const accountName = ('.bb > .fl > h1');
        const accountNameText = await page.textContent(accountName);
        expect(accountNameText).to.have.string(accountHistoryText);
    });

    it('Should transfer the money between accounts', async () => {
        const availableBalance = ('.bb > .fl > table > tbody > tr:first-child > td > table > tbody > tr:last-child > td:last-child');
        let oldAvailableBalanceNumber = await page.textContent(availableBalance);
        oldAvailableBalanceNumber = oldAvailableBalanceNumber.trim().substr(1);
        await page.click('#MenuHyperLink3');
        await page.selectOption('#toAccount', '800001');
        await page.fill('#transferAmount', '100');
        await page.click('#transfer');
        await goto('http://demo.testfire.net/bank/main.jsp');
        await page.click('#btnGetAccount');
        let newAvailableBalanceNumber = await page.textContent(availableBalance);
        newAvailableBalanceNumber = newAvailableBalanceNumber.trim().substr(1);
        expect(oldAvailableBalanceNumber - newAvailableBalanceNumber).to.equal(100);
    });

    it('Should find the article with title containing "Watchfire"', async () => {
        await goto('http://demo.testfire.net/bank/main.jsp');
        await page.click('#MenuHyperLink4');
        await page.evaluate(() => {
            const example = document.querySelector('#QueryXpath #query');
            example.value = 'Watchfire';
        });
        await page.click('#Button1');
        const results = await page.textContent('#QueryXpath');
        expect(results).to.have.string('Watchfire');
    });

    it('Should apply to Altoro Mutual Gold Visa Application', async () => {
        await goto('http://demo.testfire.net/bank/main.jsp');
        await page.click('a[href="apply.jsp"]');
        const altoroMutualGoldVisaApplication = ('.bb > .fl > h1');
        const altoroMutualGoldVisaApplicationText = await page.textContent(altoroMutualGoldVisaApplication);
        expect(altoroMutualGoldVisaApplicationText).to.have.string('Altoro Mutual Gold Visa Application');
    });

});